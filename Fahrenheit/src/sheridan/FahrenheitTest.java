package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	@Test
	public void testConvertFromCelsiusReg() {
		int num = Fahrenheit.convertFromCelsius(0);
		System.out.println(num);
		assertTrue("The Temp is correct", num == 32);
		
	}
	@Test(expected = NumberFormatException.class)
	public void testConvertFromCelsiusNeg() {
		int num = Fahrenheit.convertFromCelsius(2);
		System.out.println(num);
		assertTrue("The Temp is incorrect", num != 32 );	
	}
	@Test
	public void testConvertFromCelsiusBoundaryIn() {
		int num = Fahrenheit.convertFromCelsius(0);
		System.out.println(num);
		assertTrue("The Temp is in correct", num == 32 );	
	}
	@Test (expected = NumberFormatException.class)
	public void testConvertFromCelsiusBoundaryOut() {
		int num = Fahrenheit.convertFromCelsius(2);
		System.out.println(num);
		assertTrue("The Temp is out of bound", num == 29.9 );	
	}

}	


