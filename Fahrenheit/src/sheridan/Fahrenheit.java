package sheridan;

public class Fahrenheit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(convertFromCelsius(0));
	}
	
	public static int convertFromCelsius(int cNum)throws NumberFormatException {
		double fNum=0.0;
		double cNum2=0.0;
		fNum  = cNum * 1.8 + 32;		
		int rFNum = (int) (Math.ceil(fNum));
		cNum2 = ((5*(rFNum - 32.00))/9.0);
		int rCNum = (int) (Math.ceil(cNum2));
		if(rCNum==cNum) {
			return rFNum;	
		}
		else {
			throw new NumberFormatException();
		}
		
	}

}
